var passport = require('../configuration/passport');

module.exports = function(app, passport) {
    
    app.route('/')
    .get(function (req,res) {
        res.send('Parabens, ta rodando certinho');
    });
    
    /////////////////////// AUTENTICACAO ////////////////////////////
    
    app.route('/auth/login/success')
    .get(function (req,res) {
        res.redirect('/')
    });
    
    app.route('/auth/login/failure')
    .get(function (req,res) {
        res.redirect('/');
    });
    
    app.route('/auth/facebook')
    .get(passport.authenticate('facebook', { scope : 'email'}));

    app.route('/auth/facebook/callback')
    .get(passport.authenticate('facebook', {
        successRedirect: '/auth/login/success',
        failureRedirect: '/auth/login/failure'
    }));
    
    app.route('/auth/google')
    .get(passport.authenticate('google', { scope : ['profile', 'email'] }));

    app.route('/auth/google/callback')
    .get(passport.authenticate('google', {
            successRedirect : '/auth/login/success',
            failureRedirect : '/auth/login/failure'
    }));
     
    app.route('/auth/logout')
    .get(function(req, res) {
        req.logout();
        res.redirect('/');
    });
    ///////////////////////////////////////////////////////////////////
};